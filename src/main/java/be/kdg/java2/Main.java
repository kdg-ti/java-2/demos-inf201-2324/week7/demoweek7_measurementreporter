package be.kdg.java2;

import be.kdg.java2.presentation.Menu;
import be.kdg.java2.service.JSONMeasurementsService;
import be.kdg.java2.service.MeasurementsService;
import be.kdg.java2.service.XMLMeasurementsService;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello gradle!");
        //MeasurementsService measurementsService = new XMLMeasurementsService();
        MeasurementsService measurementsService = new JSONMeasurementsService();
        Menu menu = new Menu(measurementsService);
        menu.show();
    }
}
