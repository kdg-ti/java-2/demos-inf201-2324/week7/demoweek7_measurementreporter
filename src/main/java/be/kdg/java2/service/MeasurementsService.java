package be.kdg.java2.service;

import be.kdg.java2.domain.Measurement;

import java.util.List;

public interface MeasurementsService {
    List<Measurement> getMeasurementsForDevice(int deviceId);
}
