package be.kdg.java2.service;

import be.kdg.java2.domain.Measurement;
import be.kdg.java2.util.GSonLocalDateTimeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class JSONMeasurementsService implements MeasurementsService {
    @Override
    public List<Measurement> getMeasurementsForDevice(int deviceId) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LocalDateTime.class,
                new GSonLocalDateTimeAdapter().nullSafe());
        Gson gson = gsonBuilder.create();
        try (BufferedReader bufferedReader
                     = new BufferedReader(new FileReader("device" + deviceId + ".json"))) {
            Measurement[] measurements = gson.fromJson(bufferedReader, Measurement[].class);
            return Arrays.asList(measurements);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
