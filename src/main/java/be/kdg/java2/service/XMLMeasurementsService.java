package be.kdg.java2.service;

import be.kdg.java2.domain.Measurement;
import be.kdg.java2.domain.Measurements;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

import java.io.File;
import java.util.List;

public class XMLMeasurementsService implements MeasurementsService {
    @Override
    public List<Measurement> getMeasurementsForDevice(int deviceId){
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Measurements.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            File file = new File("device" + deviceId + ".xml");
            Measurements measurements = (Measurements) unmarshaller.unmarshal(file);
            return measurements.getMeasurements();
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }

    }
}
