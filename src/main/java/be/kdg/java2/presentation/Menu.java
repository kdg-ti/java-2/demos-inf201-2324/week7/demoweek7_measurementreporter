package be.kdg.java2.presentation;

import be.kdg.java2.service.MeasurementsService;

import java.util.Scanner;

public class Menu {
    private MeasurementsService measurementsService;

    public Menu(MeasurementsService measurementsService) {
        this.measurementsService = measurementsService;
    }

    public void show(){
        System.out.println("Reporting Measurements");
        System.out.println("======================");
        while (true) {
            System.out.println("Geef een deviceId:");
            Scanner scanner = new Scanner(System.in);
            int deviceId = scanner.nextInt();
            showReport(deviceId);
        }
    }

    private void showReport(int deviceId) {
        measurementsService.getMeasurementsForDevice(deviceId)
                .forEach(measurement -> {
                    if (measurement.getHumidity()>50) {
                        System.out.println("ALERT: humidity to high at "
                                + measurement.getTimestamp());
                    }
                    if (measurement.getTemperature()<13) {
                        System.out.println("ALERT: temperature to low at "
                                + measurement.getTimestamp());
                    }
                    if (measurement.getCoConcentration()>1900) {
                        System.out.println("ALERT: CO concentration to high at "
                                + measurement.getTimestamp());
                    }
                });
    }
}
